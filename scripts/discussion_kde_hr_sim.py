from mymodules import activity_access as aa
from probabilistic_simulation.kde.kde_conditoning import KdeConditioner, BinCounter
from probabilistic_simulation.prob_hr_simulation import TrainingDataObject, TestDataObject
import matplotlib.pyplot as plt
import numpy as np
from definitions import vocabularies as vocs
plt.style.use('ggplot')


def plot_pdf(coords):
    if coords.dim == 3:
        fig, ax = plt.subplots()
        #ax.pcolormesh(coords.X, coords.Y, coords.Z, shading='gouraud', cmap=plt.cm.BuGn_r)
        ax.contour(coords.X, coords.Y, coords.Z, linewidths=2)
        ax.set_xlabel(coords.x_axis_lbl)
        ax.set_ylabel(coords.y_axis_lbl)
        return fig, ax
    elif coords.dim == 2:
        fig, ax = plt.subplots()
        ax.plot(coords.X, coords.Y)
        ax.set_xlabel(coords.x_axis_lbl)
        ax.set_ylabel(vocs['density'])
        return fig, ax
    else:
        raise Exception('Wrong Dimension!')


#%% setup experiments --------------------------------------------------------------------------------------------------
act_dao = aa.get_activity_dao('Ken Bach', external_storage=True)
act_ids = act_dao.filter_ids_by_flags(act_dao.fetch_ids_from_months(2019, [7, 8]), pwr=True, hr=True)
hist_conf = {'p_30': (30, 'ewma'), 'hr_30': (30, 'ewma'),
             'p_120': (120, 'ewma'), 'hr_120': (120, 'ewma'),
             'p_1200': (1200, 'ewma'), 'hr_1200': (1200, 'ewma'),
             'p_3600': (60*60, 'ewma'), 'hr_3600': (60*60, 'ewma')}
training_data = TrainingDataObject(act_dao, act_ids, hist_conf)
X_df = training_data.dataset.data_all
test_data = TestDataObject(act_dao, act_id=465, history_conf=hist_conf)
# ----------------------------------------------------------------------------------------------------------------------
#%% Example: Conditioning a multidimensional density distribution w/ ONE random vars -----------------------------------
# basic example w/ one random var and any conditioned random var -------------------------------------------------------
conditional_row = {'hr_30': 125, 'p_30': 546}

kde_cond = KdeConditioner(X_df, var_names=['hr_now'], conditionals_dict=conditional_row, hist_conf=hist_conf)
# show conditional information
bc = BinCounter()
bin_counts = bc.count_bins(kde_cond.hist_tbl, kde_cond.now_tbl)
cond_info = bc.get_conditional_info(conditional_row)
# calc the pdf and and plot it
coords = kde_cond.calc_densities_kde(bandwidth=8)
fig, ax = plot_pdf(coords)
if cond_info is not None:
    ax.scatter(cond_info.rv_realisations, np.full(len(cond_info.rv_realisations), 0), c='red', marker='x')
    ax.legend([r'$' + kde_cond.get_f_latex() + '$', f"{vocs['observations']} (n={cond_info.counts})"])
else:
    ax.legend([r'$' + kde_cond.get_f_latex() + '$', f"{vocs['observations']} (n=0)"])
ax.grid(True)
fig.tight_layout()
# ----------------------------------------------------------------------------------------------------------------------
#%% Example: Conditioning a multidimensional density distribution w/ TWO random vars -----------------------------------
# basic example w/ one random var and any conditioned random var -------------------------------------------------------

#conditional_row = training_data.dataset.data_history.iloc[1200].to_dict()  # conditionals from test activitiy
conditional_row = {'hr_30': 170}  # manual conditioning
kde_cond = KdeConditioner(X_df, var_names=['p_30', 'hr_now'], conditionals_dict=conditional_row,
                          hist_conf=hist_conf)
bc = BinCounter()
bin_counts = bc.count_bins(kde_cond.hist_tbl, kde_cond.now_tbl)
# pick a conditional in 'bin_count' table
#row_idx = 20
#conditional_row = bin_counts.iloc[row_idx, 0:-1].to_dict()
#for bin_key, bin_val in conditional_row.items():
#    conditional_row[bin_key] = bin_val.mid
#kde_cond = KdeConditioner(X_df, var_names=['p_now', 'hr_now'], conditionals_dict=conditional_row,
#                          hist_conf=hist_conf)

# show conditional information
cond_info = bc.get_conditional_info(conditional_row)
# calc the pdf and and plot it
coords = kde_cond.calc_densities_kde(bandwidth=4)
fig, ax = plot_pdf(coords)
if cond_info is not None:
    X = np.array(cond_info.rv_realisations)
    ax.scatter(X[:, 0], X[:, 1], c='red', marker='x', s=2)
    ax.legend([f"{vocs['observations']} (n={cond_info.counts})"])
ax.set_title(r'$' + kde_cond.get_f_latex() + '$')
ax.grid(True)
ax.set_ylim([90, 180])
fig.tight_layout()
# ----------------------------------------------------------------------------------------------------------------------
#%% Experiment: Cardiac Drift 2D ---------------------------------------------------------------------------------------
bandwidth = 8  # adjust bandwidth to see the difference of predictor's effect
kde_cond = KdeConditioner(dataframe=X_df,
                          var_names=['hr_now'],
                          conditionals_dict={'p_1200': 200, 'p_120': 200, 'hr_1200': 135},
                          hist_conf=hist_conf)
coords1 = kde_cond.calc_densities_kde(bandwidth)

kde_cond = KdeConditioner(dataframe=X_df,
                          var_names=['hr_now'],
                          conditionals_dict={'p_1200': 200, 'p_120': 200, 'hr_1200': 145},
                          hist_conf=hist_conf)
coords2 = kde_cond.calc_densities_kde(bandwidth)
fig, ax = plt.subplots()
ax.plot(coords1.X, coords1.Y)
ax.plot(coords2.X, coords2.Y)
# ----------------------------------------------------------------------------------------------------------------------
#%% Experiment: Cardiac Drift 3D ---------------------------------------------------------------------------------------
kde_cond = KdeConditioner(dataframe=X_df,
                          var_names=['hr_1200', 'hr_now'],
                          conditionals_dict={'p_1200': 200, 'p_120': 200},
                          hist_conf=hist_conf)
coords1 = kde_cond.calc_densities_kde(bandwidth=8)

kde_cond = KdeConditioner(dataframe=X_df,
                          var_names=['hr_1200', 'hr_now'],
                          conditionals_dict={'p_1200': 200, 'p_120': 200},
                          hist_conf=hist_conf)
coords2 = kde_cond.calc_densities_kde(bandwidth=15)
plot_pdf(coords1)
plot_pdf(coords2)
# cardiac Drift funzt zwar schon, aber es fehlt eine Akkumulator Variable sodass der Loop durchbrochen werden kann
# ----------------------------------------------------------------------------------------------------------------------
#%% Experiment: Effect of 4th dimension, Issue with sprints ------------------------------------------------------------
p_conditional = 546
conditional_rows = ({'hr_30': 125, 'p_30': p_conditional}, {'hr_30': 135, 'p_30': p_conditional})
# 2-dimensional case
for conditional_row in conditional_rows:
    kde_cond = KdeConditioner(X_df, var_names=['hr_now'], conditionals_dict=conditional_row, hist_conf=hist_conf)
    # fetch conditional information (data points)
    bc = BinCounter()
    bin_counts = bc.count_bins(kde_cond.hist_tbl, kde_cond.now_tbl)
    cond_info = bc.get_conditional_info(conditional_row)
    # calc the pdf and and plot it
    coords = kde_cond.calc_densities_kde(bandwidth=8)
    fig, ax = plot_pdf(coords)
    if cond_info is not None:
        ax.scatter(cond_info.rv_realisations, np.full(len(cond_info.rv_realisations), 0), c='red', marker='x')
        ax.legend([r'$' + kde_cond.get_f_latex() + '$', f"{vocs['observations']} (n={cond_info.counts})"],
                  loc='upper right')
    else:
        ax.legend([r'$' + kde_cond.get_f_latex() + '$', f"{vocs['observations']} (n=0)"])
    ax.grid(True)
    fig.tight_layout()

# 3-dimensional case
conditional_rows = ({'hr_30': 125}, {'hr_30': 135})
for conditional_row in conditional_rows:
    kde_cond = KdeConditioner(X_df, var_names=['p_30', 'hr_now'], conditionals_dict=conditional_row, hist_conf=hist_conf)
    # fetch conditional information (data points)
    bc = BinCounter()
    bin_counts = bc.count_bins(kde_cond.hist_tbl, kde_cond.now_tbl)
    cond_info = bc.get_conditional_info(conditional_row)
    # calc the pdf and and plot it
    coords = kde_cond.calc_densities_kde(bandwidth=8, x_grid=range(0, 400, 15))
    fig, ax = plot_pdf(coords)
    if cond_info is not None:
        X = np.array(cond_info.rv_realisations)
        ax.axvline(x=p_conditional, c='tab:blue', ls='--')
        ax.scatter(X[:, 0], X[:, 1], c='red', marker='x', s=5)
        ax.legend([f'${kde_cond.get_f_latex()[2:40]}$={p_conditional}', f"{vocs['observations']} (n={cond_info.counts})"])
    ax.set_title(r'$' + kde_cond.get_f_latex() + '$')
    ax.grid(True)
    ax.axvline(c='grey', lw=1)
    ax.set_ylim([80, 180])
    ax.set_xlim([0, 820])
    fig.tight_layout()

# ----------------------------------------------------------------------------------------------------------------------
#%% Experiment: Effect of 4th dimension by 'Moving conditional variable' -----------------------------------------------
# TODO: try out different bandwidths
conditional_rows = [{'hr_30': 130},
                    {'hr_30': 150},
                    {'hr_30': 170}]
ranges = [range(50, 300, 10), range(80, 400, 10), range(75, 800, 15)]

fig, ax = plt.subplots()
for i, conditional_row in enumerate(conditional_rows):
    kde_cond = KdeConditioner(X_df, var_names=['p_30', 'hr_now'], conditionals_dict=conditional_row,
                              hist_conf=hist_conf)
    # calc the pdf and and plot it
    coords = kde_cond.calc_densities_kde(bandwidth=8, x_grid=ranges[i])
    ax.contour(coords.X, coords.Y, coords.Z, linewidths=2, colors=f'C{i}')
    ax.axhline(y=0, c=f'C{i}', label=f'${kde_cond.get_f_latex()}$')
ax.set_xlabel(coords.x_axis_lbl)
ax.set_ylabel(coords.y_axis_lbl)
#ax.set_title(r'$' + kde_cond.get_f_latex() + '$')
ax.grid(True)
ax.set_ylim([100, 220])
ax.set_xlim([0, 600])
ax.legend(loc='upper left')
fig.tight_layout()
# ----------------------------------------------------------------------------------------------------------------------
