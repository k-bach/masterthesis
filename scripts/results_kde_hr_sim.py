from probabilistic_simulation.prob_hr_simulation import i_to_kw
import matplotlib
from matplotlib import pyplot as plt
from matplotlib.ticker import MultipleLocator
from probabilistic_simulation.kde.kde_hr_simulation_evaluation import TestConfiguration, KdeEvaluation
import numpy as np
import pandas as pd
#plt.style.use('ggplot')


# region FUNCTIONS
def heatmap(data, row_labels, col_labels, ax=None, plot_cbar=True, cbar_kw={}, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (N, M).
    row_labels
        A list or array of length N with the labels for the rows.
    col_labels
        A list or array of length M with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    if plot_cbar:
        cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
        cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")
    else:
        cbar = None

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    # Let the horizontal axes labeling appear on top.
    #ax.tick_params(top=False, bottom=True,
    #               labeltop=False, labelbottom=True)

    # Rotate the tick labels and set their alignment.
    #plt.setp(ax.get_xticklabels(), rotation=-30, ha="right",
    #         rotation_mode="anchor")

    # Turn spines off and create white grid.
    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar


def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
                     textcolors=["black", "white"],
                     threshold=None, data2=None, **textkw):
    """
    A function to annotate a heatmap.

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    valfmt
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}", or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors
        A list or array of two color specifications.  The first is used for
        values below a threshold, the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **kwargs
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
            if data2 is not None:
                text = im.axes.text(j, i, f"{valfmt(data[i, j], None)}\n+/- {valfmt(data2[i, j], None)}", **kw)
            else:
                text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)
    return texts


def plot_box_or_violin(data, x_ax, box_or_violin, xlbl, ylbl, ax=None):
    # plot sse per bw
    N = len(x_ax)
    ind = np.arange(N)  # the x locations for the groups
    if ax is None:
        fig = plt.figure()
        ax = fig.add_subplot()
    if box_or_violin == 2:
        ax.violinplot(data, showmeans=False, showmedians=True, showextrema=True)
    elif box_or_violin == 1:
        ax.boxplot(data)
    else:
        return
    ax.set_xticks(ind + 1)
    ax.set_xticklabels(x_ax)
    ax.yaxis.grid(True, which='both')
    ax.set_xlabel(xlbl)
    ax.set_ylabel(ylbl)
    return ax


def feed_heatmaps_unbiased(n, m, my_athletes, attributes, my_fun, *my_fun_args):
    K = len(my_athletes)
    arr_means, arr_std = np.zeros((n, m, K)), np.zeros((n, m, K))
    for i in range(n):
        bw = i_to_kw('bw', i)
        for j in range(m):
            hc = i_to_kw('hc', j)
            for k, athlete_name in enumerate(my_athletes):
                # perform specific operations on the fetched attributes
                fetch_results = my_fun([athlete_name], [hc], [bw], attributes, *my_fun_args)
                # aggregate the lists
                fetch_results_arr = np.array(fetch_results)
                arr_means[i, j, k] = np.mean(fetch_results_arr)
                arr_std[i, j, k] = np.std(fetch_results_arr)
    return arr_means, arr_std


def compute_total_means(arr_means_3d, arr_std_3d):
    n, m, K = arr_means_3d.shape[0], arr_means_3d.shape[1], arr_means_3d.shape[2]
    arr_means_2d = np.zeros((n, m))
    arr_std_2d = np.zeros((n, m))
    for i in range(n):
        for j in range(m):
            for k in range(K):
                arr_means_2d[i][j] = np.mean(arr_means_3d[i, j, 0:K])
                arr_std_2d[i][j] = np.mean(arr_std_3d[i, j, 0:K])
    return arr_means_2d, arr_std_2d


def compute_athlete_means(arr):
    arr_means_3d = arr[0]
    n, m, K = arr_means_3d.shape[0], arr_means_3d.shape[1], arr_means_3d.shape[2]
    arr_means_2d = np.zeros((n, m))
    arr_std_2d = np.zeros((n, m))
    for i in range(n):
        for j in range(m):
            for k in range(K):
                arr_means_2d[i][j] = np.mean(arr_means_3d[i, j, 0:K])
                arr_std_2d[i][j] = np.std(arr_means_3d[i, j, 0:K])
    return arr_means_2d, arr_std_2d


def hm_out_of_bounds(athlete_names, hc, bw, attributes, *args):
    filtered_keys = kde_eval.filter_results(athlete=athlete_names, hc=hc, bw=bw)
    prob_list = kde_eval.fetch_attributes_from_key_lists(filtered_keys, attributes)
    lower_bar, upper_bar = args[0], args[1]
    out_of_bounds_list = list()
    for probs in prob_list:
        out_of_bounds_list.append(np.sum((probs > upper_bar) | (probs < lower_bar)) / np.sum(probs))
    return out_of_bounds_list


def hm_rmse(athlete_names, hc, bw, attributes, *args):
    filtered_keys = kde_eval.filter_results(athlete=athlete_names, hc=hc, bw=bw)
    errors_list = kde_eval.fetch_attributes_from_key_lists(filtered_keys, attributes)
    return [np.sqrt(np.mean(np.power(errors, 2))) for errors in errors_list]


def hm_costs(athlete_names, hc, bw, attributes, *args):
    filtered_keys = kde_eval.filter_results(athlete=athlete_names, hc=hc, bw=bw)
    cost_list = kde_eval.fetch_attributes_from_key_lists(filtered_keys, attributes)
    n_samples = np.array([len(hr_mu) for hr_mu in kde_eval.fetch_attributes_from_key_lists(filtered_keys, ['hr_mu'])])
    return (np.array(cost_list) * 1000) / n_samples


def hm_ci_sizes(athlete_names, hc, bw, attributes, *args):
    filtered_keys = kde_eval.filter_results(athlete=athlete_names, hc=hc, bw=bw)
    ci_sizes_list = kde_eval.fetch_attributes_from_key_lists(filtered_keys, attributes)
    return [np.mean(ci_sizes) for ci_sizes in ci_sizes_list]


def hm_signed_errors(athlete_names, hc, bw, attributes, *args):
    filtered_keys = kde_eval.filter_results(athlete=athlete_names, hc=hc, bw=bw)
    signed_errors_list = kde_eval.fetch_attributes_from_key_lists(filtered_keys, attributes)
    return [np.mean(signed_errors) for signed_errors in signed_errors_list]


# endregion


# region SETUP TESTS --------------------------------------------------------------------------------------------------
#%%
kde_eval = KdeEvaluation()
athlete_names = ['ken_bach', 'david', 'dr', 's5a1', 's5a2', 's5a3', 's5a4']
filename = 'p_only'
results = dict()
for athlete_name in athlete_names:
    kde_eval.load_results(athlete_name, filename=filename, use_external=True)
    results[athlete_name] = kde_eval.results[athlete_name]
kde_eval = KdeEvaluation(results)
bandwidths = kde_eval.results[athlete_names[0]]['test_configuration']['bandwidths']
# endregion

# region HEATMAPS -----------------------------------------------------------------------------------------------------
#%% Bandwidth vs. History: RMSE
my_athletes = ['ken_bach']
#my_athletes = ['ken_bach', 'david', 'dr', 's5a1', 's5a2', 's5a3', 's5a4']
N, M = 4, 4

show_relatives = True
for my_athlete in my_athletes:
    mean_arr, std_arr = compute_total_means(*feed_heatmaps_unbiased(N, M, [my_athlete], ['errors'], hm_rmse))
    if show_relatives:
        mean_arr = (mean_arr / np.min(mean_arr)) * 100
    # plot means
    fig, ax = plt.subplots()
    if show_relatives:
        im, cbar = heatmap(np.array(mean_arr), bandwidths, np.arange(1, 5), ax=ax,
                           cmap="YlGn", cbarlabel="RMSE")
        annotate_heatmap(im, valfmt="{x:.0f} %")
    else:
        im, cbar = heatmap(np.array(mean_arr), bandwidths, np.arange(1, 5), ax=ax,
                           cmap="YlGn", cbarlabel="RMSE")
        annotate_heatmap(im, valfmt="{x:.1f}", data2=np.array(std_arr))
    ax.set_ylabel('Bandwidth')
    ax.set_xlabel('History Configuration Nr.')
    ax.yaxis.grid(False)
    ax.xaxis.grid(False)
    fig.tight_layout()
    plt.show()
#%% Bandwidth vs. History: Out of bounds
my_athletes = ['ken_bach', 'david', 'dr', 's5a1', 's5a2', 's5a3', 's5a4']
N, M = 4, 4
for my_athlete in my_athletes:
    oob_bw_hc_mean, oob_bw_hc_median, oob_bw_hc_std = \
        feed_heatmaps_unbiased(N, M, [my_athlete], ['probabilities'], hm_out_of_bounds, *(0.05, 0.95))

    # plot means
    fig, ax = plt.subplots()
    im, cbar = heatmap(np.array(oob_bw_hc_mean)*100, bandwidths, np.arange(1, 5), ax=ax,
                       plot_cbar=False, cmap="YlGn")
    annotate_heatmap(im, valfmt="{x:.0f} %")
    ax.set_ylabel('Bandwidth')
    ax.set_xlabel('History Configuration Nr.')
    fig.tight_layout()
    plt.show()

#%% Bandwidth vs. History: Cost
my_athletes = ['ken_bach', 'david', 'dr', 's5a1', 's5a2', 's5a3', 's5a4']
N, M = 4, 4
for my_athlete in my_athletes:
    cost_bw_hc_mean, cost_bw_hc_median, cost_bw_hc_std = feed_heatmaps_unbiased(N, M, ['david'], ['cost'], hm_costs)
    # plot means
    fig, ax = plt.subplots()
    im, cbar = heatmap(np.array(cost_bw_hc_mean), bandwidths, np.arange(1, 5), ax=ax,
                       cmap="YlGn", cbarlabel="Zeitkosten pro Prognose [ms]")
    annotate_heatmap(im, valfmt="{x:.0f}ms", data2=np.array(cost_bw_hc_std))
    ax.set_ylabel('Bandwidth')
    ax.set_xlabel('History Configuration Nr.')
    fig.tight_layout()
    plt.show()
#%% Bandwidth vs. History: CI sizes
my_athletes = ['ken_bach']#, 'david', 'dr', 's5a1', 's5a2', 's5a3', 's5a4']
N, M = 4, 4

show_relatives = False
for my_athlete in my_athletes:
    ci_bw_hc_mean, _, ci_bw_hc_std = feed_heatmaps_unbiased(N, M, [my_athlete], ['ci_sizes'], hm_ci_sizes)
    if show_relatives:
        ci_bw_hc_mean = (ci_bw_hc_mean / np.min(ci_bw_hc_mean)) * 100
    # plot means
    fig, ax = plt.subplots()
    if show_relatives:
        im, cbar = heatmap(np.array(ci_bw_hc_mean), bandwidths, np.arange(1, 5), ax=ax,
                           cmap="YlGn", cbarlabel="Größe des 95% Konfidenzintervalls [%]")
        annotate_heatmap(im, valfmt="{x:.0f} %")

    else:
        im, cbar = heatmap(np.array(ci_bw_hc_mean), bandwidths, np.arange(1, 5), ax=ax,
                           cmap="YlGn", cbarlabel="Größe des 95% Konfidenzintervalls [bpm]")
        annotate_heatmap(im, valfmt="{x:.0f} bpm", data2=np.array(ci_bw_hc_std))
    ax.set_ylabel('Bandwidth')
    ax.set_xlabel('History Configuration Nr.')
    fig.tight_layout()
    ax.grid()
    plt.show()

#%% Bandwidth vs. History: Signed errors
my_athletes = ['ken_bach']#, 'david', 'dr', 's5a1', 's5a2', 's5a3', 's5a4']
N, M = 4, 4

for my_athlete in my_athletes:
    mu, std = feed_heatmaps_unbiased(N, M, [my_athlete], ['errors'], hm_signed_errors)
    # plot means
    fig, ax = plt.subplots()
    im, cbar = heatmap(np.array(mu), bandwidths, np.arange(1, 5), ax=ax, cmap="YlGn", cbarlabel="")
    annotate_heatmap(im, valfmt="{x:.0f} bpm")
    ax.set_ylabel('Bandwidth')
    ax.set_xlabel('History Configuration Nr.')
    ax.yaxis.grid(False)
    ax.xaxis.grid(False)
    fig.tight_layout()
    plt.show()

#%%
# endregion

# region VIOLIN / BOX PLOTS --------------------------------------------------------------------------------------------
#%% probabilities per bw
probs_per_bw = kde_eval.combine_arrays_per_kw('bw', [0, 1, 2, 3], 'ken_bach', 'probabilities', hc=3)
plot_box_or_violin(probs_per_bw, bandwidths, 2, 'KDE Bandwidth', 'Probability')

#%% ci sizes per bw
ci_sizes_per_bw = kde_eval.combine_arrays_per_kw('bw', [0, 1, 2, 3], 'Ken_Bach', 'ci_sizes')
plot_box_or_violin(ci_sizes_per_bw, bandwidths, 2, 'KDE Bandwidth', 'CI size')

#%% root mean square errors per bw
rmse_per_bw = list()
for i in range(4):
    bw = i_to_kw('bw', i)
    filtered_keys = kde_eval.filter_results(athlete=['ken_bach'], hc=['hc#3'], bw=[bw])
    rmse_per_bw.append(kde_eval.fetch_attributes_from_key_lists(filtered_keys, ['rmse']))
plot_box_or_violin(rmse_per_bw, bandwidths, 2, 'KDE Bandwidth', 'Root Mean Square Error (RMSE)')

#%% out of bounds per bw
bws = [i_to_kw('bw', i) for i in range(4)]
filtered_keys = kde_eval.filter_results(athlete=['Ken_Bach'], bw=bws)
out_of_bounds_data = pd.concat(objs=[kde_eval.fetch_meta_from_key_lists(filtered_keys),
                                     kde_eval.fetch_out_of_bounds(filtered_keys)],
                               axis=1)

N = len(bandwidths)
ind = np.arange(N)  # the x locations for the groups
below_portion, above_portion = np.zeros((N,), float), np.zeros((N,), dtype=float)
portions_per_activity = np.zeros((int(len(out_of_bounds_data) / N), N), dtype=float)
bar_width = 0.35

for i in range(N):
    data = out_of_bounds_data.loc[out_of_bounds_data.bw_idx == i, :]
    below_portion[i] = data.below.sum() / data.total.sum()
    above_portion[i] = data.above.sum() / data.total.sum()
    portions_per_activity[:, i] = data.both / data.total

fig = plt.figure()
ax = fig.add_subplot()
p1 = ax.bar(ind, below_portion * 100, bar_width)
p2 = ax.bar(ind, above_portion * 100, bar_width, bottom=below_portion * 100)
plt.xticks(ind, test_conf.bandwidths)
ax.grid(axis='y')
ax.set_xlabel('KDE bandwidth')
ax.set_ylabel('%')
plt.legend((p1[0], p2[0]), (r'$\widehat {hr} < 95\% CI$', r'$\widehat {hr} > 95\% CI$'))

# TODO Give it a name...
ax = plot_box_or_violin(portions_per_activity * 100, bandwidths, 2, 'KDE Bandwidth', '%')
ax.yaxis.set_minor_locator(MultipleLocator(5))
ax.yaxis.grid(True, which='both')
# ----------------------------------------------------------------------------------------------------------------------
# endregion
