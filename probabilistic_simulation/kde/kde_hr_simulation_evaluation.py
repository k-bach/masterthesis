from probabilistic_simulation.prob_hr_simulation import TrainingDataObject, TestDataObject, Evaluation
from probabilistic_simulation.kde.kde_hr_simulation import SimulationMode
import mymodules.activity_access as aa
import numpy as np
import multiprocessing as mp
import definitions
from probabilistic_simulation.kde.kde_hr_simulation import KdeHrSimulation, HrSimulationResults
from functools import reduce


class TestSet:
    def __init__(self, act_dao, ts_id, validation_set, history, bandwidth, sampling_rate, eval_grid):
        self.act_dao = act_dao
        self.ts_id = ts_id
        self.act_ids_train = validation_set[0]
        self.act_ids_test = validation_set[1]
        self.act_ids_total = validation_set[0] + validation_set[1]
        self.bandwidth = bandwidth
        self.sampling_rate = sampling_rate
        self.eval_grid = eval_grid
        self.hist_conf_id = history[0]
        self.hist_conf = history[1]
        train_data_obj = TrainingDataObject(act_dao, self.act_ids_total, self.hist_conf)
        self.period_lbl = train_data_obj.period_lbl
        self.period_span = train_data_obj.period_span
        self.n_activities_total = len(self.act_ids_total)
        self.n_activities_test = len(self.act_ids_test)
        self.n_activities_train = self.n_activities_total-self.n_activities_test
        self.results = dict()

    def get_test_setup_as_dict(self):
        test_setup = vars(self).copy()
        test_setup.pop('act_ids_total')
        test_setup.pop('results')
        test_setup.pop('act_dao')
        return test_setup

    def perform_test_runs(self):
        train_data_obj = TrainingDataObject(self.act_dao, self.act_ids_train, self.hist_conf)
        sim = KdeHrSimulation(train_data_obj, mode=SimulationMode.DYNAMIC)
        sim.fit_kde(bandwidth=self.bandwidth, eval_grid=self.eval_grid)
        self.results[self.ts_id] = dict()
        self.results[self.ts_id]['setup'] = dict()
        self.results[self.ts_id]['setup'] = self.get_test_setup_as_dict()
        self.results[self.ts_id]['results'] = dict()
        for i in range(self.n_activities_test):
            test_data_obj = TestDataObject(self.act_dao, self.act_ids_test[i], self.hist_conf)
            test_run_lbl = f'test#{i}'
            sim_results = sim.simulate_activity(test_data_obj=test_data_obj,
                                                sampling_rate=self.sampling_rate).get_results_as_dict()
            self.results[self.ts_id]['results'][test_run_lbl] = sim_results


class TestConfiguration:
    def __init__(self, act_dao=None, act_ids=None, bandwidths=None, rel_runs_per_set=None,
                 sampling_rate=None, eval_grid=None, hist_confs=None):
        self.act_dao = act_dao
        self.bandwidths = bandwidths
        self.rel_runs_per_set = rel_runs_per_set
        self.sampling_rate = sampling_rate
        self.eval_grid = eval_grid
        self.hist_confs = hist_confs
        self.act_ids = act_ids
        if act_dao is not None and act_ids is not None:
            self.friendly_athlete_name = self.act_dao.get_friendly_athlete_name()
            self.athlete_name = self.act_dao.athlete_name
            self.validation_sets = self.leave_n_out()
        else:
            self.friendly_athlete_name = None
            self.athlete_name = None
            self.validation_sets = None

    def test_conf_to_dict(self):
        d = vars(self).copy()
        d.pop('act_dao')
        d['eval_grid'] = (self.eval_grid.start, self.eval_grid.stop, self.eval_grid.step)
        return d

    def set_attributes_by_dict(self, attr_dict):
        for k, v in attr_dict.items():
            setattr(self, k, v)

    def leave_n_out(self):
        validation_sets = list()
        for i in range(len(self.act_ids)):
            ids_training = self.act_ids[i].copy()
            ids_test = list()
            n_runs = int(np.round(len(ids_training)*self.rel_runs_per_set))

            for j in range(n_runs):
                idx = -1
                while idx == -1:
                    idx = np.random.randint(0, len(ids_training), 1)[0]
                    act_id = ids_training[idx]
                    n_datapoints = self.act_dao.get_activity_by_id(act_id).data.shape[0]
                    if n_datapoints > 60*60:
                        ids_test.append(ids_training[idx])
                        ids_training.remove(ids_training[idx])
                    else:
                        # reject activity as its not long enough for our purposes. choose another one randomly
                        # TODO While loop is not safe against infity loops. Fix this in the future
                        idx = -1
            validation_sets.append((ids_training, ids_test))
        return validation_sets

    def start_testing(self, seq_or_par=0):
        results = dict()
        c = 1
        params = list()
        results[self.friendly_athlete_name] = dict()
        results[self.friendly_athlete_name]['test_configuration'] = self.test_conf_to_dict()
        for hc in range(len(self.hist_confs)):
            results[self.friendly_athlete_name][f'hc#{hc}'] = dict()
            for bw in range(len(self.bandwidths)):
                results[self.friendly_athlete_name][f'hc#{hc}'][f'bw#{bw}'] = dict()
                c_test_set = 1
                for ai in range(len(self.act_ids)):
                    results[self.friendly_athlete_name][f'hc#{hc}'][f'bw#{bw}'][f'ts#{ai}'] = dict()
                    results[self.friendly_athlete_name][f'hc#{hc}'][f'bw#{bw}'][f'ts#{ai}']['results'] = dict()
                    suffix = str(c_test_set) if c_test_set >= 10 else '0' + str(c_test_set)
                    ts_id = 'ts_' + suffix
                    c_test_set = c_test_set+1
                    test_set = TestSet(self.act_dao, ts_id, self.validation_sets[ai], (hc, self.hist_confs[hc]),
                                       self.bandwidths[bw], self.sampling_rate, self.eval_grid)
                    results[self.friendly_athlete_name][f'hc#{hc}'][f'bw#{bw}'][f'ts#{ai}']['setup'] = \
                        test_set.get_test_setup_as_dict()
                    for ti in range(len(self.validation_sets[ai][1])):
                        results[self.friendly_athlete_name][f'hc#{hc}'][f'bw#{bw}'][f'ts#{ai}']['results'][f'test#{ti}'] = dict()
                        params.append([c, hc, bw, ai, ti])
                        c = c+1

        if seq_or_par == 1:
            # perform evaluations on simulations sequentially
            res = [self.perform_test_runs(*p) for p in params]
        else:
            # perform evaluations on simulations in parallel
            pool = mp.Pool(mp.cpu_count())
            res = pool.starmap(self.perform_test_runs, [p for p in params])
            print('All test runs have been performed!')
            pool.close()

        # gather all results
        for r in res:
            bw = f"bw#{r['param_ids']['bw']}"
            hc = f"hc#{r['param_ids']['hc']}"
            ts = f"ts#{r['param_ids']['ts']}"
            t = f"{r['param_ids']['t']}"
            results[self.friendly_athlete_name][hc][bw][ts]['results'][t] = r
        return results

    def perform_test_runs(self, c, hc, bw, ai, ti):
        print(f'performing #{c}')
        test_id = 'test#' + str(ti)
        train_data_obj = TrainingDataObject(self.act_dao, self.validation_sets[ai][0], self.hist_confs[hc])
        sim = KdeHrSimulation(train_data_obj, mode=SimulationMode.DYNAMIC)
        sim.fit_kde(bandwidth=self.bandwidths[bw], eval_grid=self.eval_grid)
        test_data_obj = TestDataObject(self.act_dao, self.validation_sets[ai][1][ti], self.hist_confs[hc])
        results = sim.simulate_activity(test_data_obj=test_data_obj,
                              sampling_rate=self.sampling_rate).get_results_as_dict()
        results['param_ids'] = {'bw': bw, 'hc': hc, 'ts': ai, 't': 'test#' + str(ti)}
        return results


class KdeEvaluation(Evaluation):
    def __init__(self, results=None):
        kind = 'kde'
        meta_columns = ['athlete', 'hc_idx', 'bw_idx', 'ts_idx', 'tst_idx']
        meta_idx = [0, 1, 2, 3, 5]
        super().__init__(kind, results, meta_columns, meta_idx)

    def filter_results(self, athlete=None, hc=None, bw=None, ts=None, tst=None):
        # Code looks messy but works pretty well
        athlete_keys = [k for k in self.results.keys()] if athlete is None else athlete
        hc_keys = [k for k in self.results[athlete_keys[0]].keys()] if hc is None else hc
        bw_keys = [k for k in self.results[athlete_keys[0]][hc_keys[0]].keys()] if bw is None else bw
        ts_keys = [k for k in self.results[athlete_keys[0]][hc_keys[0]][bw_keys[0]].keys()] if ts is None else ts

        key_list = list()
        for athlete_key in athlete_keys:
            for hc_key in hc_keys:
                for bw_key in bw_keys:
                    for ts_key in ts_keys:
                        if ts_key in self.results[athlete_key][hc_key][bw_key].keys():
                            for tst_key in self.results[athlete_key][hc_key][bw_key][ts_key]['results']:
                                if (tst is None) or (tst is not None and tst_key in tst):
                                    key_list.append([athlete_key, hc_key, bw_key, ts_key, 'results', tst_key])
        return key_list

    def fetch_result_object_from_key_list(self, key_list):
        return HrSimulationResults(reduce(dict.get, key_list, self.results))


