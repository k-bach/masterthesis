from probabilistic_simulation.prob_hr_simulation import (Dataset, HrSimulationResults, plot_simulation,
                                                         TrainingDataObject, TestDataObject)
from mymodules.kde_wrapper import KdeObject1d
from enum import Enum
import mymodules.activity_access as aa
from scipy import interpolate
import numpy as np
import pandas as pd
import time


class KdeHrSimulationResults(HrSimulationResults):
    def __init__(self, attr_dict=None, train_data_obj=None, test_data_obj=None, sampling=None,
                 hr_mu=None, hr_05=None, hr_95=None, probabilities=None, cost=None, nan_cases=None):
        super().__init__(attr_dict, train_data_obj, test_data_obj, sampling,
                         hr_mu, hr_05, hr_95, probabilities, cost, nan_cases)


class SimulationMode(Enum):
    # The different modes in which a KDE model can be fitted and conditionalised
    PN_HN = 0  # (Power: None, Heart Rate: None) no historic data is being used
    PH_HN = 1  # (Power: Historic, Heart Rate: None) use historic power only
    PH_HH = 2  # (Power: Historic, Heart Rate: Historic) use both historic power and hr
    PH_HS = 3  # (Power: Historic, Heart Rate: Simulated) use historic power and predicted/simulated HR
    DYNAMIC = 4  # Looks automatically which mode could be applied based on the assigned historic dataset in train_data


class KdeHrSimulation:
    def __init__(self, train_data_obj, mode):
        self.train_data_obj = train_data_obj
        self.test_data_obj = None
        self.kde_obj = None
        self.results = None
        if mode == SimulationMode.DYNAMIC:
            if self.train_data_obj.dataset.get_data_history(p=True, hr=True) is not None:
                self.mode = SimulationMode.PH_HS
            elif self.train_data_obj.dataset.get_data_history(p=True, hr=False) is not None:
                self.mode = SimulationMode.PH_HN
            elif self.train_data_obj.dataset.get_data_history(p=False, hr=True) is not None:
                pass  # TODO This case doesn't make sense. throw exception.
            else:
                self.mode = SimulationMode.PN_HN
        else:
            self.mode = mode
        self.no_hr_hist = True if self.mode == SimulationMode.PN_HN else False
        self.no_hist = True if self.mode == SimulationMode.PN_HN else False

    def fit_kde(self, bandwidth, eval_grid, rtol=0.1):
        self.kde_obj = KdeObject1d(X=self.train_data_obj.dataset.data_all.values, eval_grid=eval_grid)
        self.kde_obj.fit_kde(bandwidth, **{'rtol': rtol})

    def simulate_activity(self, test_data_obj, sampling_rate=5, refinement=8, plot_debug=False):
        if self.mode == SimulationMode.PH_HS:
            if sampling_rate != 1:
                raise Exception(f"The sampling rate in mode {self.mode} must be equal to 1!")

        self.test_data_obj = test_data_obj
        sampling = np.arange(0, len(self.test_data_obj.dataset.data_all), sampling_rate)
        hr_grid_fine = np.linspace(self.kde_obj.eval_grid[0],
                                   self.kde_obj.eval_grid[-1],
                                   len(self.kde_obj.eval_grid) * refinement)
        hr_mu = np.zeros((len(sampling),))
        hr_05 = np.zeros((len(sampling),))
        hr_95 = np.zeros((len(sampling),))
        probabilities = np.zeros((len(sampling),))
        p = self.test_data_obj.dataset.data_now.iloc[:, 0]
        hr = self.test_data_obj.dataset.data_now.iloc[:, 1]
        t_min = 20*60  # min. time the model uses the ground truth (hr-) data

        #if test_data_obj.history_conf is not None:
        #    conditionals_log = np.zeros((len(sampling), len(test_data_obj.history_conf)))
        nan_cases = list()
        start = time.time()
        for i, s in enumerate(sampling):
            if plot_debug:
                print(f'{i+1} / {len(sampling)}')

            conditionals = None
            if not self.no_hist:
                if self.no_hr_hist:  # => model uses historic pwr only
                    p_hist = self.test_data_obj.dataset.get_data_history(p=True, hr=False)
                    conditionals = p_hist.iloc[s, :].values
                else:
                    if self.mode == SimulationMode.PH_HS:  # => model uses historic pwr AND simulated hr
                        if i < t_min:
                            # In this case the model works on historic (ground truth) hr data
                            conditionals = self.test_data_obj.dataset.data_history.iloc[i, :].values
                        else:
                            # TODO: The recomputation of hr history for each prediction is expensive. As only one additional
                            #  value comes on top of the list, consider to update the approach with an online algorithm.
                            # In this case the model works on historic hr data which have been predictions in previous
                            # iterations
                            conditionals = Dataset().calc_history(p.iloc[:i],
                                                                  pd.Series(hr_mu[:i], name='hr_now'),
                                                                  test_data_obj.history_conf).values[-1, :]
                    elif self.mode == SimulationMode.PH_HH:
                        p_hist = self.test_data_obj.dataset.get_data_history(p=True, hr=True)
                        conditionals = p_hist.iloc[s, :].values
                    elif self.mode == SimulationMode.PH_HN:
                        p_hist = self.test_data_obj.dataset.get_data_history(p=True, hr=False)
                        conditionals = p_hist.iloc[s, :].values

                    if np.isnan(conditionals).any():
                        # if the model doesn't know how to handle the data we have to cheat
                        #if test_data_obj.history_conf is not None:
                            #nan_cases.append((i, p[s], conditionals_log[i - 1]))
                        conditionals = self.test_data_obj.dataset.data_history.iloc[i, :].values
            pdf = self.kde_obj.calc_pdf(p=p[s], conditionals=conditionals)

            # calc mu, s2 and store them
            f = interpolate.interp1d(self.kde_obj.eval_grid, pdf, kind='linear')
            pdf_s = f(hr_grid_fine)
            if (np.sum(pdf_s) == 0) or (np.sum(pdf_s) == np.nan):
                # In this case the KDE model might have no clue what to do as it has never seen any data of this kind
                hr_mu[i] = np.NaN
                hr_05[i] = np.NaN
                hr_95[i] = np.NaN
                probabilities[i] = np.NaN
            else:
                pdf_cs = np.cumsum(pdf_s / np.sum(pdf_s))
                hr_mu[i] = hr_grid_fine[np.sum(pdf_cs <= 0.5)]
                hr_05[i] = hr_grid_fine[np.sum(pdf_cs <= 0.05)]
                hr_95[i] = hr_grid_fine[np.sum(pdf_cs <= 0.95)]
                idx = np.sum(hr_grid_fine < hr[sampling[i]])
                idx = len(pdf_cs)-1 if idx >= len(pdf_cs) else idx
                probabilities[i] = pdf_cs[idx]  # spot of real hr in pdf curve
        end = time.time()

        sim_res = KdeHrSimulationResults(train_data_obj=self.train_data_obj,
                                         test_data_obj=self.test_data_obj,
                                         sampling=range(0, len(self.test_data_obj.dataset.data_all), sampling_rate),
                                         hr_mu=hr_mu, hr_05=hr_05, hr_95=hr_95,
                                         probabilities=probabilities,
                                         cost=end-start,
                                         nan_cases=nan_cases)
        if plot_debug:
            print(f'Time elapsed: {end-start:0.2f}s')
        self.results = sim_res
        return sim_res


if __name__ == '__none__':  # TODO set to main
    history0 = None
    history1 = {'p_30': (30, 'ewma')}
    history2 = {'p_60': (60, 'ewma'), 'p_90': (90, 'ewma'), 'p_180': (180, 'ewma')}
    history3 = {'p_30': (30, 'ewma'), 'p_90': (90, 'ewma'), 'p_120': (120, 'uniform'), 'p_240': (240, 'uniform')}
    history4 = {'p_30': (30, 'ewma'), 'p_60': (60, 'ewma'), 'p_90': (90, 'ewma'), 'p_120': (120, 'ewma')}
    history5 = {'p_30': (30, 'uniform'), 'p_60': (60, 'uniform'), 'p_90': (90, 'uniform'), 'p_120': (120, 'uniform')}
    history6 = {'p_30': (30, 'ewma'), 'p_60': (60, 'ewma'), 'p_120': (120, 'ewma'), 'p_300': (300, 'ewma'),
                'p_1200': (1200, 'ewma'),
                'hr_30': (30, 'uniform'), 'hr_60': (60, 'ewma'), 'hr_120': (120, 'ewma'), 'hr_300': (300, 'ewma'),
                'hr_1200': (1200, 'ewma')}
    act_dao = aa.get_activity_dao('Ken Bach', external_storage=False)
    selected_history = history0
    act_ids = act_dao.filter_ids_by_flags(act_dao.fetch_ids_from_months(2019, [7, 8]), pwr=True, hr=True)
    train_obj = TrainingDataObject(act_dao, act_ids, selected_history)

    test_act_id = act_dao.pick_best_test_candidate(act_dao.fetch_ids_from_dtstr('2019-06-20'))
    #test_act_id = 447
    test_obj = TestDataObject(act_dao, test_act_id, selected_history)

    sim1 = KdeHrSimulation(train_obj, SimulationMode.DYNAMIC)
    sim1.fit_kde(bandwidth=8, eval_grid=range(100, 195, 10))
    sim1.simulate_activity(test_obj, sampling_rate=5, plot_debug=True)

    #act_ids = act_dao.filter_ids_by_flags(act_dao.fetch_ids_from_months(2018, [9, 10]), pwr=True, hr=True)
    #train_obj = TrainingDataObject(act_dao, act_ids, selected_history)
    #sim2 = KdeHrSimulation(train_obj, SimulationMode.DYNAMIC)
    #sim2.fit_kde(bandwidth=8, eval_grid=range(100, 195, 10))
    #sim2.simulate_activity(test_obj, sampling_rate=1, plot_debug=True)

    plot_simulation([sim1.results], plot_ci=True, plot_truth=True)

