from sklearn.neighbors import KernelDensity
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from definitions import vocabularies as vocs
plt.style.use('ggplot')


class Coords2d:
    def __init__(self, x_grid, x_axis_lbl=None):
        self._dim = 2
        self.X = np.array(x_grid)
        self.Y = None
        self._x_grid_idx = dict(zip(self.X, np.arange(len(self.X))))
        self._x_axis_lbl = x_axis_lbl

    @property
    def dim(self):
        return self._dim

    @property
    def x_axis_lbl(self):
        return self._x_axis_lbl


class Coords3d:
    def __init__(self, x_grid, y_grid, x_axis_lbl=None, y_axis_lbl=None):
        self._dim = 3
        self.X, self.Y = np.mgrid[x_grid, y_grid]
        self.Z = None
        self._x_grid = np.array(x_grid)
        self._y_grid = np.array(x_grid)
        self._x_grid_idx = dict(zip(self._x_grid, np.arange(len(self._x_grid))))
        self._y_grid_idx = dict(zip(self._y_grid, np.arange(len(self._y_grid))))
        self._x_axis_lbl = x_axis_lbl
        self._y_axis_lbl = y_axis_lbl

    def compute_y_modes(self):
        if self.Z is not None:
            self.y_modes = np.zeros(self._x_grid.shape, dtype=int)
            for i in range(len(self._x_grid)):
                pdf = self.Z[i]
                self.y_modes[i] = self._y_grid[pdf.argmax()]
            return self.y_modes
        else:
            print('First you need to compute the Kernel!')
            return None

    def compute_y_means(self):
        if self.Z is not None:
            self.y_means = np.zeros(self._x_grid.shape, dtype=int)
            for i in range(len(self._x_grid)):
                pdf = self.Z[i]
                pdf_norm = pdf / pdf.sum()
                cs = pdf_norm.cumsum()
                mean_idx = len(pdf_norm[cs <= 0.5])
                self.y_means[i] = self._y_grid[mean_idx]
            return self.y_means
        else:
            print('First you need to compute the Kernel!')
            return None

    def get_x_conditioned_pdf(self, x):
        idx = self._x_grid_idx[x]
        return self.Z[idx, :]

    def get_y_conditioned_pdf(self, y):
        idx = self._y_grid_idx[y]
        return self.Z[:, idx]

    def get_x_indices(self, p):
        a = set(self._x_grid)
        b = set(p)
        intersect = list(a.intersection(b))
        intersect.sort()
        ret = list()
        for i in intersect:
            ret.append(self._x_grid_idx[i])
        return ret

    def get_y_indices(self, hr):
        a = set(self._y_grid)
        b = set(hr)
        l = list(a.intersection(b)).sort()
        return l

    @property
    def dim(self):
        return self._dim

    @property
    def x_axis_lbl(self):
        return self._x_axis_lbl

    @property
    def y_axis_lbl(self):
        return self._y_axis_lbl


class ConditionedKde:
    def __init__(self):
        self._bandwidth = None
        self._kde_model = None
        self._conditionals = None
        self._coords = None

    def fit_model(self, X, bandwidth, **kwargs):
        self._bandwidth = bandwidth
        self._kde_model = KernelDensity(bandwidth=bandwidth, **kwargs)
        self._kde_model.fit(X)

    @property
    def coords(self):
        return self._coords


class ConditionedKde2d(ConditionedKde):
    def __init__(self, coords):
        super().__init__()
        self._dimensions = 2
        self._coords = coords

    def calc_pdf(self, conditionals=None):
        self._conditionals = conditionals
        positions = self.coords.X[np.newaxis]
        if conditionals is not None:
            for c in conditionals:
                positions = np.vstack([positions, np.full(positions[0, :].shape, c)])
        log_pdf = self._kde_model.score_samples(positions.T)
        self._coords.Y = np.reshape(np.exp(log_pdf), self._coords.X.shape)


class ConditionedKde3d(ConditionedKde):
    def __init__(self, coords):
        super().__init__()
        self._dimensions = 3
        self._coords = coords

    def calc_pdf(self, conditionals=None):
        self._conditionals = conditionals
        positions = np.vstack([self._coords.X.ravel(),
                               self._coords.Y.ravel()])
        if conditionals is not None:
            for c in conditionals:
                positions = np.vstack([positions, np.full(positions[0, np.newaxis].shape, c)])
        log_pdf = self._kde_model.score_samples(positions.T)
        self._coords.Z = np.reshape(np.exp(log_pdf), self._coords.X.shape)


class KdeConditioner:
    def __init__(self, dataframe, var_names, conditionals_dict, hist_conf):
        self.hist_conf = hist_conf
        self._dimensions = len(var_names)+1
        self._var_names = var_names
        self._dataframe = dataframe
        self._conditionals_dict = conditionals_dict
        self._n_conditionals = len(conditionals_dict.keys())
        self._kde_obj = None
        self._x_axis_lbl = None
        self._y_axis_lbl = None
        self._x_grid_default = None
        self._y_grid_default = None
        self._setup_axis(var_names)

    def get_f_latex(self):
        hist_conf_slim = dict()
        for var_name, cond_value in self._conditionals_dict.items():
            hist_conf_slim[var_name] = (self.hist_conf[var_name][0], self.hist_conf[var_name][1], cond_value)

        c = 0
        output = 'f('
        for var_name in self._var_names:
            if var_name.endswith('now'):
                output += var_name[0:var_name.find('_')].upper()+'(t)'
            else:
                t = self.hist_conf[var_name][0]
                ma_mode = 'EMA' if self.hist_conf[var_name][1] == 'ewma' else 'MA'
                var_name = var_name[0:var_name.find('_')].upper()
                output += r'\mathit{{{var_name}}}^{{(d={t}s)}}_{{\mathit{{{ma}}}}}(t)'.format(var_name=var_name, t=t, ma=ma_mode)
            if c+1 < len(self._var_names):
                output += ', '

        if len(hist_conf_slim) > 0:
            c = 0
            output += r' \mid '
            for var_name, var_conf in hist_conf_slim.items():
                v = var_name[0:var_name.index('_')].upper()
                t = var_conf[0]
                ma_mode = 'EMA' if var_conf[1] == 'ewma' else 'MA'
                cond_value = var_conf[2]
                if c > 0:
                    output += r', \mathit{{{var_name}}}^{{(d={t}s)}}_{{\mathit{{{ma}}}}}(t)={cond_value}'.format(
                        var_name=v, t=t, ma=ma_mode, cond_value=cond_value)
                else:
                    output += r'\mathit{{{var_name}}}^{{(d={t}s)}}_{{\mathit{{{ma}}}}}(t)={cond_value}'.format(
                        var_name=v, t=t, ma=ma_mode, cond_value=cond_value)
                c = c + 1
        output += ')'
        return output

    def _setup_axis(self, var_names):
        if var_names[0].startswith('p'):
            self._x_grid_default = range(100, 400, 5)
            self._x_axis_lbl = f"{vocs['power']}"
        elif var_names[0].startswith('hr'):
            self._x_grid_default = range(100, 200, 1)
            self._x_axis_lbl = f"{vocs['heartrate']}"
        if self._dimensions == 3:
            if var_names[0].startswith('p'):
                self._x_grid_default = range(100, 400, 15)
                self._x_axis_lbl = f"{vocs['power']}"
            elif var_names[0].startswith('hr'):
                self._x_grid_default = range(100, 200, 5)
                self._x_axis_lbl = f"{vocs['heartrate']}"
            if var_names[1].startswith('p'):
                self._y_grid_default = range(100, 400, 15)
                self._y_axis_lbl = f"{vocs['power']}"
            elif var_names[1].startswith('hr'):
                self._y_grid_default = range(100, 200, 5)
                self._y_axis_lbl = f"{vocs['heartrate']}"

    def calc_densities_kde(self, bandwidth, x_grid=None, y_grid=None, **opt_fit_args):
        x_grid = self._x_grid_default if x_grid is None else x_grid
        y_grid = self._y_grid_default if (y_grid is None and self._dimensions == 3) else y_grid
        if self._dimensions == 2:
            self._kde_obj = ConditionedKde2d(Coords2d(x_grid, self._x_axis_lbl))
        elif self._dimensions == 3:
            self._kde_obj = ConditionedKde3d(Coords3d(x_grid, y_grid, self._x_axis_lbl, self._y_axis_lbl))
        self._kde_obj.fit_model(self.X.values, bandwidth, **opt_fit_args)
        self._kde_obj.calc_pdf(list(self._conditionals_dict.values()))
        return self._kde_obj.coords

    @property
    def hist_tbl(self):
        return self._dataframe.loc[:, list(self._conditionals_dict.keys())]

    @property
    def now_tbl(self):
        return self._dataframe.loc[:, self._var_names]

    @property
    def X(self):
        return self._dataframe.loc[:, self._var_names+list(self._conditionals_dict.keys())]


class BinCounter:
    def __init__(self, p_bins=None, hr_bins=None):
        self.p_bins = p_bins if p_bins is not None else np.arange(80, 700 + 1, 10)
        self.hr_bins = hr_bins if hr_bins is not None else np.arange(90, 200 + 1, 4)
        self.bin_counts_df = None
        self.kv_store_counts = None
        self.kv_store_indices = None
        self.dataset = None
        self.df_now = None

    def cut_bins(self, df):
        n_cols, n_rows = df.shape[1], df.shape[0]
        # cut p_hr_tbl data into bins
        bin_tbl = pd.DataFrame()
        for i in range(n_cols):
            if df.columns[i].startswith('p'):
                cat_obj = pd.cut(x=df.iloc[:, i], bins=self.p_bins, labels=np.arange(len(self.p_bins) - 1))
                #cat_obj = pd.cut(x=df.iloc[:, i], bins=self.p_bins)
            elif df.columns[i].startswith('hr'):
                cat_obj = pd.cut(x=df.iloc[:, i], bins=self.hr_bins, labels=np.arange(len(self.hr_bins) - 1))
                #cat_obj = pd.cut(x=df.iloc[:, i], bins=self.hr_bins)
            bin_tbl.insert(loc=i, column=df.columns[i], value=cat_obj)
        bin_tbl.dropna(inplace=True)
        return bin_tbl

    def count_bins(self, df_hist, df_now=None):
        self.df_now = df_now
        n_cols, n_rows = df_hist.shape[1], df_hist.shape[0]
        bin_tbl = self.cut_bins(df_hist)
        # count how often bin combinations occur
        arr = np.array(bin_tbl.values, dtype=np.int8)
        indices = bin_tbl.index.values
        #arr = np.array(bin_tbl.values)
        self.kv_store_counts = dict()
        self.kv_store_indices = dict()
        for i in range(len(arr)):
            key = tuple(arr[i, :])
            counts = self.kv_store_counts.get(key)
            if counts is None:
                self.kv_store_counts[key] = 1
                self.kv_store_indices[key] = [indices[i]]
            else:
                self.kv_store_counts[key] += 1
                self.kv_store_indices[key].append(indices[i])

        bin_counts = np.zeros((len(self.kv_store_counts), n_cols+1), dtype=np.int64)
        #bin_counts = np.zeros((len(self.kv_store_counts), n_cols + 1), dtype=object)
        c = 0
        for k, v in self.kv_store_counts.items():
            bin_counts[c, 0:n_cols] = k
            bin_counts[c, n_cols] = v
            c += 1

        bin_counts = bin_counts[bin_counts[:, n_cols].argsort()[::-1]]
        cols = bin_tbl.columns.values.tolist() + ['bin_count']
        self.bin_counts_df = pd.DataFrame(data=bin_counts, columns=cols)
        return self.bin_counts_df

    def get_conditional_info(self, conditional_row):
        bins = tuple(self.cut_bins(pd.DataFrame(data=conditional_row, index=[0])).values[0])
        try:
            counts = self.kv_store_counts[bins]
            indices = self.kv_store_indices[bins]
            if self.df_now is not None:
                variables = self.df_now.iloc[indices].values
                if self.df_now.shape[1] == 1:
                    variables = variables.ravel().tolist()
                else:
                    variables = variables.tolist()
                cond_info = pd.Series(data={'conditional_rv_names': tuple(conditional_row.keys()),
                                            'conditional_rv_realisations': tuple(conditional_row.values()),
                                            'bins': bins,
                                            'counts': counts,
                                            'rv_names': tuple(self.df_now.keys()),
                                            'rv_realisations': variables},
                                      name='Conditional info')
            else:
                cond_info = pd.Series(data={'conditional_rv_names': tuple(conditional_row.values()),
                                            'conditional_rv_realisations': tuple(conditional_row.values()),
                                            'bins': bins,
                                            'counts': counts,
                                            'rv_indices': indices},
                                      name='Conditional info')
            return cond_info
        except KeyError:
            print('There are no datapoints in the surrounding!')
            return None
