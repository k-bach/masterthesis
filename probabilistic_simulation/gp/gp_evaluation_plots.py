from probabilistic_simulation.prob_hr_simulation import i_to_kw
from matplotlib import pyplot as plt
from matplotlib.ticker import MultipleLocator
import numpy as np


def plot_box_or_violin(data, x_ax, box_or_violin, xlbl, ylbl, ax=None):
    # plot sse per bw
    N = len(x_ax)
    ind = np.arange(N)  # the x locations for the groups
    if ax is None:
        fig = plt.figure()
        ax = fig.add_subplot()
    if box_or_violin == 2:
        ax.violinplot(data, showmeans=False, showmedians=True, showextrema=True)
    elif box_or_violin == 1:
        ax.boxplot(data)
    else:
        return
    ax.set_xticks(ind + 1)
    ax.set_xticklabels(x_ax)
    ax.yaxis.grid(True, which='both')
    ax.set_xlabel(xlbl)
    ax.set_ylabel(ylbl)
    return ax


def plot_rmse_per_ls(rmse_per_kn, length_scales):
    plot_box_or_violin(rmse_per_kn, length_scales, 2, 'Characteristic Length Scale', 'Root Mean Square Error (RMSE)')


def plot_ci_sizes_per_nl(ci_sizes, noise_levels):
    plot_box_or_violin(ci_sizes, noise_levels, 2, 'Noise Level', 'CI size')


def plot_out_of_bounds(out_of_bounds_data, noise_levels):
    N = len(noise_levels)
    ind = np.arange(N)  # the x locations for the groups
    below_portion, above_portion = np.zeros((N,), float), np.zeros((N,), dtype=float)
    portions_per_activity = np.zeros((int(len(out_of_bounds_data) / N), N), dtype=float)
    bar_width = 0.35

    for i in range(N):
        data = out_of_bounds_data.loc[out_of_bounds_data.nl_idx == i, :]
        below_portion[i] = data.below.sum() / data.total.sum()
        above_portion[i] = data.above.sum() / data.total.sum()
        portions_per_activity[:, i] = data.both / data.total

    fig = plt.figure()
    ax = fig.add_subplot()
    p1 = ax.bar(ind, below_portion * 100, bar_width)
    p2 = ax.bar(ind, above_portion * 100, bar_width, bottom=below_portion * 100)
    plt.xticks(ind, noise_levels)
    ax.grid(axis='y')
    ax.set_xlabel('KDE bandwidth')
    ax.set_ylabel('%')
    plt.legend((p1[0], p2[0]), (r'$\widehat {hr} < 95\% CI$', r'$\widehat {hr} > 95\% CI$'))

    # TODO Give it a name...
    ax = plot_box_or_violin(portions_per_activity * 100, noise_levels, 2, 'Noise Level', '%')
    ax.yaxis.set_minor_locator(MultipleLocator(5))
    ax.yaxis.grid(True, which='both')

