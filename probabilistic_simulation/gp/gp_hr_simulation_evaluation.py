from sklearn.gaussian_process.kernels import RBF, ConstantKernel, WhiteKernel
import mymodules.activity_access as aa
import numpy as np
from probabilistic_simulation.prob_hr_simulation import Evaluation, i_to_kw
from functools import reduce
from probabilistic_simulation.prob_hr_simulation import TrainingDataObject, TestDataObject
from probabilistic_simulation.gp.gp_hr_simulation import HrSimulation, GpSimulationResults
from probabilistic_simulation.gp import gp_evaluation_plots as plots
from probabilistic_simulation.gp.gp_hr_simulation import plot_simulation
import pandas as pd


class TestSet:
    def __init__(self, ts_id, validation_set, history, kernel, sample_rate):
        self.ts_id = ts_id
        self.act_ids_train = validation_set[0]
        self.act_ids_test = validation_set[1]
        self.act_ids_total = validation_set[0] + validation_set[1]
        self.kernel = kernel
        self.sample_rate = sample_rate
        self.hist_conf_id = history[0]
        self.hist_conf = history[1]
        train_data_obj = TrainingDataObject(self.act_ids_total, self.hist_conf)
        self.period_lbl = train_data_obj.period_lbl
        self.period_span = train_data_obj.period_span
        self.n_activities_total = len(self.act_ids_total)
        self.n_activities_test = len(self.act_ids_test)
        self.n_activities_train = self.n_activities_total-self.n_activities_test
        self.results = dict()

    def get_test_setup_as_dict(self):
        test_setup = vars(self).copy()
        test_setup.pop('act_ids_total')
        test_setup.pop('results')
        return test_setup

    def perform_test_runs(self):
        train_data_obj = TrainingDataObject(self.act_ids_train, self.hist_conf)
        sim = HrSimulation(train_data_obj)
        sim.fit_gp(self.kernel, self.sample_rate)
        self.results[self.ts_id] = dict()
        self.results[self.ts_id]['setup'] = dict()
        self.results[self.ts_id]['setup'] = self.get_test_setup_as_dict()
        self.results[self.ts_id]['results'] = dict()
        for i in range(self.n_activities_test):
            test_data_obj = TestDataObject(self.act_ids_test[i], self.hist_conf)
            test_run_lbl = f'test#{i}'
            sim_results = sim.simulate_activity(test_data_obj=test_data_obj).get_results_as_dict()
            self.results[self.ts_id]['results'][test_run_lbl] = sim_results


class TestConfiguration:
    def __init__(self):
        self.sample_rate = 15
        self.length_scales = [10, 50, 100]
        self.noise_levels = [20, 40, 60]
        self.rel_runs_per_set = 0.25
        self.hist_confs = [{'p_30': (30, 'ewma'), 'p_90': (90, 'ewma'), 'p_240': (240, 'ewma'), 'p_1200': (1200, 'xp')}]
        act_ids = [aa.act_dao.filter_ids_by_flags(aa.act_dao.fetch_ids_from_months(2017, [6, 7]), pwr=True, hr=True),
                   aa.act_dao.filter_ids_by_flags(aa.act_dao.fetch_ids_from_months(2017, [8, 9]), pwr=True, hr=True),
                   aa.act_dao.filter_ids_by_flags(aa.act_dao.fetch_ids_from_months(2019, [4, 5]), pwr=True, hr=True),
                   aa.act_dao.filter_ids_by_flags(aa.act_dao.fetch_ids_from_months(2019, [6, 7]), pwr=True, hr=True),
                   aa.act_dao.filter_ids_by_flags(aa.act_dao.fetch_ids_from_months(2019, [8, 9]), pwr=True, hr=True),
                   aa.act_dao.filter_ids_by_flags(aa.act_dao.fetch_ids_from_months(2019, [11, 12]), pwr=True, hr=True)]
        self.act_ids = act_ids
        self.validation_sets = self.leave_n_out()

    def leave_n_out(self):
        validation_sets = list()
        for i in range(len(self.act_ids)):
            ids_training = self.act_ids[i].copy()
            ids_test = list()
            n_runs = int(np.round(len(ids_training)*self.rel_runs_per_set))

            for j in range(n_runs):
                idx = np.random.randint(0, len(ids_training), 1)[0]
                ids_test.append(ids_training[idx])
                ids_training.remove(ids_training[idx])
            validation_sets.append((ids_training, ids_test))
        return validation_sets

    def start_testing(self):
        results = dict()
        c = 1
        params = list()
        results[aa.get_friendly_athlete_name()] = dict()
        for hc in range(len(self.hist_confs)):
            results[aa.get_friendly_athlete_name()][f'hc#{hc}'] = dict()
            for ls in range(len(self.length_scales)):
                results[aa.get_friendly_athlete_name()][f'hc#{hc}'][f'ls#{ls}'] = dict()
                for nl in range(len(self.noise_levels)):
                    results[aa.get_friendly_athlete_name()][f'hc#{hc}'][f'ls#{ls}'][f'nl#{nl}'] = dict()
                    for ai in range(len(self.act_ids)):
                        results[aa.get_friendly_athlete_name()][f'hc#{hc}'][f'ls#{ls}'][f'nl#{nl}'][f'ts#{ai}'] = dict()
                        params.append([c, ai, hc, ls, nl])
                        c = c+1

        res = [self.perform_test_runs(*p) for p in params]

        # gather all results
        for r in res:
            k, v = r.popitem()
            nl = f"nl#{v['param_ids']['nl']}"
            ls = f"ls#{v['param_ids']['ls']}"
            hc = f"hc#{v['param_ids']['hc']}"
            ts = f"ts#{v['param_ids']['ts']}"
            results[aa.get_friendly_athlete_name()][hc][ls][nl][ts] = v
        return results

    def perform_test_runs(self, c, ai, hc, ls, nl):
        print(f'performing #{c}')
        suffix = str(c) if c >= 10 else '0' + str(c)
        ts_id = 'ts_' + suffix
        kernel = 200 * RBF(length_scale=self.length_scales[ls]) + ConstantKernel(100) + WhiteKernel(self.noise_levels[nl])
        test_set = TestSet(ts_id=ts_id,
                           validation_set=self.validation_sets[ai],
                           history=(hc, self.hist_confs[hc]),
                           kernel=kernel,
                           sample_rate=self.sample_rate)
        test_set.perform_test_runs()
        test_set_results = test_set.results
        test_set_results[test_set.ts_id]['param_ids'] = {'ls': ls, 'nl': nl, 'hc': hc, 'ts': ai}
        return test_set_results


class GpEvaluation(Evaluation):
    def __init__(self, results=None):
        kind = 'gp'
        meta_columns = ['athlete', 'hc_idx', 'ls_idx', 'nl_idx', 'ts_idx', 'tst_idx']
        meta_idx = [0, 1, 2, 3, 4, 6]
        super().__init__(kind, results, meta_columns, meta_idx)

    def filter_results(self, athlete=None, hc=None, ls=None, nl=None, ts=None, tst=None):
        # Code looks messy but works pretty well
        athlete_keys = [k for k in self.results.keys()] if athlete is None else athlete
        hc_keys = [k for k in self.results[athlete_keys[0]].keys()] if hc is None else hc
        ls_keys = [k for k in self.results[athlete_keys[0]][hc_keys[0]].keys()] if ls is None else ls
        nl_keys = [k for k in self.results[athlete_keys[0]][hc_keys[0]][ls_keys[0]].keys()] if nl is None else nl
        ts_keys = [k for k in self.results[athlete_keys[0]][hc_keys[0]][ls_keys[0]][nl_keys[0]].keys()] \
            if ts is None else ts
        key_list = list()
        for athlete_key in athlete_keys:
            for hc_key in hc_keys:
                for ls_key in ls_keys:
                    for nl_key in nl_keys:
                        for ts_key in ts_keys:
                            for tst_key in self.results[athlete_key][hc_key][ls_key][nl_key][ts_key]['results']:
                                if (tst is None) or (tst is not None and tst_key in tst):
                                    key_list.append([athlete_key, hc_key, ls_key, nl_key, ts_key, 'results', tst_key])
        return key_list

    def fetch_result_object_from_key_list(self, key_list):
        return GpSimulationResults(reduce(dict.get, key_list, self.results))


# if __name__ == '__main__':
#     test_conf = TestConfiguration()
#     results = test_conf.start_testing()
#     gp_eval = GpEvaluation(results)
#     gp_eval.store_athlete_results(use_external=True)
#     #gp_eval.load_athlete_results(use_external=True)

#%%
test_conf = TestConfiguration()
gp_eval = GpEvaluation()
gp_eval.load_athlete_results(use_external=True)

# %% plot ci sizes per white noise level
ci_sizes_per_bw = gp_eval.combine_arrays_per_kw('nl', [0, 1, 2], 'Ken_Bach', 'ci_sizes', **{'ls': 0})
plots.plot_ci_sizes_per_nl(ci_sizes_per_bw, test_conf.noise_levels)


# %% plot root mean square errors per ls
rmse_per_kn = list()
for i in range(3):
    ls = i_to_kw('ls', i)
    filtered_keys = gp_eval.filter_results(athlete=['Ken_Bach'], ls=[ls], nl=['nl#1'])
    rmse_per_kn.append(gp_eval.fetch_attributes_from_key_lists(filtered_keys, ['rmse']))
length_scales = [ls for ls in test_conf.length_scales]
plots.plot_rmse_per_ls(rmse_per_kn, length_scales)

# %% plot out of bounds per white noise level
nls = [i_to_kw('nl', i) for i in range(3)]
filtered_keys = gp_eval.filter_results(athlete=['Ken_Bach'], ls=['ls#1'], nl=nls)
out_of_bounds = pd.concat(objs=[gp_eval.fetch_meta_from_key_lists(filtered_keys),
                                gp_eval.fetch_out_of_bounds(filtered_keys)],
                          axis=1)
length_scales = [nl for nl in test_conf.noise_levels]
plots.plot_out_of_bounds(out_of_bounds, length_scales)


