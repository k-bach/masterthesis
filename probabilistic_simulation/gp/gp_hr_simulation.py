import mymodules.activity_access as aa
import numpy as np
import pandas as pd
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import WhiteKernel, ConstantKernel, RBF
import time
from probabilistic_simulation.prob_hr_simulation import SimulationResults, TestDataObject, TrainingDataObject
from matplotlib import pyplot as plt


def calc_history2(hr, t_delay, history_conf):
    data = np.zeros((len(hr), len(hist_conf)))
    columns = list()
    # add history data
    for i, key in enumerate(history_conf):
        t, smooth_alg = history_conf[key][0], history_conf[key][1]
        if smooth_alg == 'ewma':
            data[:, i] = hr.ewm(span=t).mean().shift(t_delay).values
            columns.append(key)
        elif smooth_alg == 'uniform':
            data[:, i] = hr.rolling(t).mean().shift(t_delay).values
            columns.append(key)
    d = pd.DataFrame(data, hr.index, columns=columns)
    return d


def prepare_df2(act_dao, act_ids, t, history_conf=None, min_pwr=None, min_hr=None):
    df = pd.DataFrame()
    try:
        for k, act_id in enumerate(act_ids):
            act = act_dao.get_activity_by_id(act_id)
            d1 = act.data.loc[:, ['power', 'heartrate']].rolling(t).mean()
            d1.columns = ['p_t', 'hr_t']
            d2 = calc_history2(act.data.loc[:, 'heartrate'], t, history_conf)
            d = pd.concat((d1, d2), axis=1)

            d.dropna(inplace=True)
            df = df.append(d, ignore_index=True)

        df = df.loc[df.iloc[:, 0] > min_pwr, :] if min_pwr else df
        df = df.loc[df.iloc[:, 1] > min_hr, :] if min_hr else df
    except AttributeError:
        print('error')
    return df


class GpSimulationResults(SimulationResults):
    def __init__(self, attr_dict=None, test_act_id=None, test_act_duration=None, sampling=None,
                 p=None, hr_truth=None, hr_mu=None, hr_05=None, hr_95=None, cost=None):
        super().__init__(attr_dict, test_act_id, test_act_duration, sampling, p, hr_truth, hr_mu, hr_05, hr_95, cost)


class HrSimulation:
    def __init__(self, train_data_obj):
        self.train_data_obj = train_data_obj
        self.test_data_obj = None
        self.gp_obj = None
        self.results = None

    def fit_gp(self, kernel, sample_rate=15):
        gp = GaussianProcessRegressor(kernel=kernel, optimizer=None, copy_X_train=False)
        df_slim = self.train_data_obj.data.iloc[range(0, len(self.train_data_obj.data), sample_rate), :]
        X = df_slim.iloc[:, [0] + list(range(2, len(df_slim.columns)))]  # TODO ugly code, replace with HR at column 0
        y = np.array(df_slim.iloc[:, 1], dtype=int)[np.newaxis].transpose()
        self.gp_obj = gp.fit(X, y)

    def simulate_activity(self, test_data_obj, sampling_rate=5, plot_debug=False):
        self.test_data_obj = test_data_obj
        sampling = np.arange(0, len(self.test_data_obj.data), sampling_rate)
        p = self.test_data_obj.data.iloc[:, 0].values
        p_hist = self.test_data_obj.data.iloc[:, 2:]
        X_test = np.hstack((p[np.newaxis].T, p_hist.values))
        hr = self.test_data_obj.data.iloc[:, 1].values
        start = time.time()
        hr_mu, hr_std = self.gp_obj.predict(X_test[sampling, :], return_std=True)
        end = time.time()

        sim_res = GpSimulationResults(None,
                                      self.test_data_obj.act_id,
                                      self.test_data_obj.act_duration,
                                      range(0, len(self.test_data_obj.data), sampling_rate),
                                      p,
                                      hr,
                                      hr_mu.ravel(),
                                      hr_mu.ravel()-2*hr_std,
                                      hr_mu.ravel()+2*hr_std,
                                      end-start)
        if plot_debug:
            print(f'Time elapsed: {end-start:0.2f}s')
        self.results = sim_res
        return sim_res


def plot_simulation(result_objs, plot_ci=False, ylim=(100, 195)):
    fig = plt.figure()
    ax1, ax2 = fig.subplots(2, 1, sharex=True)

    for result_obj in result_objs:
        sampling = np.array(result_obj.sampling)
        ax1.plot(result_obj.hr_truth, c='tab:red', ls='--')  # plot hr (ground truth)

        mid = pd.DataFrame(result_obj.hr_mu).rolling(2, min_periods=1).mean().values.ravel()
        ax1.plot(sampling,
                 mid,
                 label=f'hr_mu from months ')
        if plot_ci:
            upper = pd.DataFrame(result_obj.hr_95).rolling(2, min_periods=1).mean().values.ravel()
            lower = pd.DataFrame(result_obj.hr_05).rolling(2, min_periods=1).mean().values.ravel()
            ax1.fill_between(sampling,
                             upper,
                             lower,
                             facecolor='gray')
        ax2.plot(pd.DataFrame(result_obj.p).rolling(25, min_periods=1).mean().values, c='C0', ls='-')
    ax1.grid()
    ax1.legend()
    ax1.set_ylabel('Heart Rate [1/min]')
    ax1.set_ylim(ylim)
    ax2.grid()
    ax2.set_ylabel('Power [W]')
    ax2.set_xlabel('Time [s]')


if __name__ == '__main__':
    hist_conf = {'p_30': (30, 'ewma'), 'p_90': (90, 'ewma'), 'p_240': (240, 'ewma')}
    act_ids = aa.act_dao.filter_ids_by_flags(aa.act_dao.fetch_ids_from_months(2019, [7]), pwr=True, hr=True)
    train_obj = TrainingDataObject(act_ids, hist_conf)
    test_obj = TestDataObject(aa.act_dao.fetch_ids_from_dtstr('2019-06-20'), hist_conf)
    sim = HrSimulation(train_obj)
    kernel = 200*RBF(length_scale=50) + ConstantKernel(100) + WhiteKernel(20)
    sim.fit_gp(kernel, sample_rate=15)
    sim.simulate_activity(test_obj, sampling_rate=15)


