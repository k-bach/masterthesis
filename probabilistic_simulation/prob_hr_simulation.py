import os
import pickle
from functools import reduce
from matplotlib import pyplot as plt
from mymodules.activity_access import get_friendly_athlete_name
import definitions
import numpy as np
import pandas as pd


class Dataset:
    def __init__(self):
        self._df = None
        self._hist_conf = None
        self._act_ids = None
        self._min_pwr = None
        self._min_hr = None
        self._hist_indices = None

    def calc_history(self, p_now, hr_now, history_conf):
        self._hist_conf = history_conf
        hist_df = None
        for h, key in enumerate(history_conf):
            if key.startswith('p'):
                hist = p_now
            elif key.startswith('hr'):
                hist = hr_now
            else:
                raise KeyError(f"Key '{key}' must start with either 'p' or 'hr'")
            hist.name = key
            t, smooth_alg = history_conf[key][0], history_conf[key][1]
            if smooth_alg == 'ewma':
                hist = hist.ewm(span=t, min_periods=1200).mean()
            elif smooth_alg == 'uniform':
                hist = hist.rolling(window=t).mean()
            elif smooth_alg == 'xp':
                hist = hist.ewm(span=25, min_periods=1).mean().values.ravel()
                xp = np.full((len(hist),), np.nan, dtype=float)
                for i in range(t, len(hist)):
                    xp[i] = np.power(np.mean(np.power(hist[i-t:i], 4)), (1/4))
                hist = pd.DataFrame(xp)
            else:
                raise KeyError(f"Key '{key}' must be either 'ewma', 'uniform' or 'xp'")
            if h == 0:
                hist_df = pd.DataFrame(hist)
            else:
                hist_df.insert(loc=h, column=key, value=hist)
        return hist_df

    def prepare_df(self, act_dao, act_ids, history_conf=None, min_pwr=None, min_hr=None):
        self._act_ids = act_ids
        self._min_pwr = min_pwr
        self._min_hr = min_hr
        df = pd.DataFrame()
        for k, act_id in enumerate(act_ids):
            act = act_dao.get_activity_by_id(act_id)

            p_now = act.data.loc[:, 'power'].rolling(3).mean()
            hr_now = act.data.loc[:, 'heartrate']
            if history_conf is not None:
                d = self.calc_history(p_now, hr_now, history_conf)
                d.insert(loc=0, column='hr_now', value=hr_now)
                d.insert(loc=0, column='p_now', value=p_now)
            else:
                d = pd.concat((p_now, hr_now), axis=1)
                d.columns = ['p_now', 'hr_now']
            d.insert(loc=0, column='t', value=p_now.index)
            d.insert(loc=0, column='aid', value=act_id)

            d.dropna(inplace=True)
            df = df.append(d, ignore_index=True)

        df = df.loc[df.p_now > min_pwr, :] if min_pwr else df
        df = df.loc[df.hr_now > min_hr, :] if min_hr else df

        self._hist_indices = {'p': list(), 'hr': list()}
        for i in range(4, len(df.columns)):
            if df.columns[i].startswith('p'):
                self._hist_indices['p'].append(i)
            elif df.columns[i].startswith('hr'):
                self._hist_indices['hr'].append(i)

        self._df = df
        return self

    @property
    def hist_conf(self):
        return self._hist_conf

    @property
    def act_ids(self):
        return self._act_ids

    @property
    def min_pwr(self):
        return self._min_pwr

    @property
    def min_hr(self):
        return self._min_hr

    @property
    def data_meta(self):
        return self._df.iloc[:, 0:2]

    @property
    def data_history(self):
        if self.hist_conf is None:
            return None
        else:
            return self._df.iloc[:, 4:]

    def has_hr_columns(self):
        return False if len(self._hist_indices['hr']) == 0 else True

    def has_p_columns(self):
        return False if len(self._hist_indices['p']) == 0 else True

    def get_data_history(self, p=True, hr=False):
        if p and hr:
            if self.has_hr_columns() and self.has_p_columns():
                return self._df.iloc[:, 4:]
        elif p:
            if self.has_p_columns():
                return self._df.iloc[:, self._hist_indices['p']]
        elif hr:
            if self.has_hr_columns():
                return self._df.iloc[:, self._hist_indices['hr']]
        #if self.hist_conf is None:
        #    return None
        return None

    @property
    def data_now(self):
        return self._df.iloc[:, 2:4]

    @property
    def data_all(self):
        return self._df.iloc[:, 2:]


class TrainingDataObject:
    def __init__(self, act_dao, act_ids, history_conf):
        self.act_dao = act_dao
        self.act_ids = act_ids
        self._dataset = None
        self.history_conf = history_conf
        period_start = self.act_dao.meta.iloc[act_ids].index.min()
        period_end = self.act_dao.meta.iloc[act_ids].index.max()
        self.period_lbl = period_start.strftime('%Y/%m/%d')+' - '+period_end.strftime('%Y/%m/%d')
        tdiff = self.act_dao.meta.iloc[act_ids].index.max() - self.act_dao.meta.iloc[act_ids].index.min()
        self.period_span = tdiff.days

    def exclude_act_id(self, act_id):
        if act_id in self.act_ids:
            self.act_ids.remove(act_id)

    @property
    def dataset(self):
        if self._dataset is None:
            self._dataset = Dataset().prepare_df(self.act_dao, self.act_ids, self.history_conf)
        return self._dataset


class TestDataObject:
    def __init__(self, act_dao, act_id, history_conf):
        self.act_dao = act_dao
        self.act_id = act_id
        self._dataset = None
        self.history_conf = history_conf
        activity = self.act_dao.get_activity_by_id(self.act_id)
        self.date_lbl = activity.date_to_str()
        self.act_duration = activity.data.iloc[-1].name

    @property
    def dataset(self):
        if self._dataset is None:
            self._dataset = Dataset().prepare_df(self.act_dao, [self.act_id], self.history_conf)
        return self._dataset


class HrSimulationResults:
    def __init__(self, attr_dict=None, train_data_obj=None, test_data_obj=None, sampling=None,
                 hr_mu=None, hr_05=None, hr_95=None, probabilities=None, cost=None, nan_cases=None):
        self.errors = None
        self.n_nans = None
        self.ci_sizes = None

        if attr_dict is not None:
            self.set_attributes_by_dict(attr_dict)
        else:
            self.test_act_id = test_data_obj.act_id
            self.test_act_duration = test_data_obj.act_duration
            self.training_period_lbl = train_data_obj.period_lbl
            self.training_period_span = train_data_obj.period_span
            self.n_training_activities = len(train_data_obj.act_ids)
            self.p = test_data_obj.dataset.data_now['p_now'].values
            self.hr_truth = test_data_obj.dataset.data_now['hr_now'].values
            self.hr_mu = hr_mu
            self.hr_05 = hr_05
            self.hr_95 = hr_95
            self.cost = cost
            self.probabilities = probabilities
            self.sampling = sampling
            self.nan_cases = nan_cases
            self.set_metrics()

    def set_attributes_by_dict(self, attr_dict):
        for k, v in attr_dict.items():
            setattr(self, k, v)
        self.set_metrics()

    def set_metrics(self):
        self.n_nans = np.sum(np.isnan(self.hr_mu))
        # ignore cases in which the model could not make any prediction
        non_nan_bitmask = ~np.isnan(self.hr_mu)
        # ignore measured hrs which cannot be or wrong
        non_invalid_hr_bitmask = self.hr_truth[self.sampling] > 30
        bitmask = non_nan_bitmask & non_invalid_hr_bitmask
        hr_mu = self.hr_mu[bitmask]
        hr_95 = self.hr_95[bitmask]
        hr_05 = self.hr_05[bitmask]
        hr_truth = self.hr_truth[self.sampling]
        hr_truth = hr_truth[bitmask]
        self.errors = hr_mu - hr_truth
        self.ci_sizes = hr_95 - hr_05

    def get_results_as_dict(self):
        return vars(self)

    def get_results_as_pd_series(self):
        return pd.Series(self.get_results_as_dict())


class Evaluation:
    def __init__(self, kind, results, meta_columns, meta_indices):
        self.results = results
        self.kind = kind
        self.meta_columns = meta_columns
        self.meta_indices = meta_indices

    def store_results(self, foldername, filename, use_external=False):
        storage_dir = definitions.EXTERNAL_STORAGE_DIR if use_external else definitions.LOCAL_STORAGE_DIR
        if foldername is not None:
            storage_dir = os.path.join(storage_dir, foldername)
        os.mkdir(storage_dir) if not os.path.exists(storage_dir) else None
        pickle_out = open(os.path.join(storage_dir, filename+'.pkl'), "wb")
        pickle.dump(self.results, pickle_out)
        pickle_out.close()
        print('Store process successful!')

    def load_results(self, foldername, filename, use_external=False):
        storage_dir = definitions.EXTERNAL_STORAGE_DIR if use_external else definitions.LOCAL_STORAGE_DIR
        storage_dir = os.path.join(storage_dir, foldername, filename+'.pkl')
        self.results = pickle.load(open(storage_dir, "rb"))

    def filter_results(self, athlete, **kw):
        return list()

    def fetch_attributes_from_key_lists(self, key_lists, attr_list, filter_nans=True):
        res = list()
        for key_list in key_lists:
            res.append(self.fetch_attributes_from_key_list(key_list, attr_list, filter_nans))
        return res

    def fetch_attributes_from_key_list(self, key_list, attr_list, filter_nans=True):
        res = list()
        for attr in attr_list:
            r = reduce(dict.get, key_list + [attr], self.results)
            if filter_nans:
                if type(r) is list or type(r) is tuple:
                    r_np = np.array(r)
                    r_np = r_np[~np.isnan(r_np)]
                    r = r_np.tolist()
                    res.append(r)
                elif type(r) is np.ndarray:
                    r = r[~np.isnan(r)]
                    res.append(r)
                elif type(r) is int or type(r) is float:
                    if r is not np.nan:
                        res.append(r)
            else:
                res.append(r)
        return res[0] if len(attr_list) == 1 else tuple(res)

    def fetch_result_object_from_key_list(self, key_list):
        pass

    def combine_arrays_per_kw(self, kw_prefix, kw_indices, athlete, attr, filter_nans=True, **extra_kvs):
        attr_per_kw = list()
        for i in kw_indices:
            kw = {kw_prefix: [i_to_kw(kw_prefix, i)]}
            for k, v in extra_kvs.items():
                kw[k] = [i_to_kw(k, v)]
            filtered_keys = self.filter_results(athlete=[athlete], **kw)
            arr = self.fetch_attributes_from_key_lists(filtered_keys, [attr])
            comb_arr = np.array([])
            for a in arr:
                comb_arr = np.concatenate((comb_arr, a))
            if filter_nans:
                comb_arr = comb_arr[~np.isnan(comb_arr)]
            attr_per_kw.append(list(comb_arr))
        return attr_per_kw

    def fetch_meta_from_key_lists(self, filtered_keys):
        arr = np.zeros((len(filtered_keys), len(self.meta_columns)), dtype=object)
        for i, key in enumerate(filtered_keys):
            key_np = np.array(key)
            arr[i, 1:len(self.meta_columns)] = [kw_to_i(k) for k in key_np[self.meta_indices[1:]]]
            arr[i, 0] = key[0]
        return pd.DataFrame(arr, columns=self.meta_columns)

    def fetch_out_of_bounds(self, filtered_keys):
        fetch = self.fetch_attributes_from_key_lists(filtered_keys,
                                                     ['hr_05', 'hr_95', 'hr_truth', 'sampling'])
        arr = np.zeros((len(fetch), 4), dtype=int)
        for i, f in enumerate(fetch):
            hr_05 = np.array(np.round(f[0]), dtype=int)
            hr_95 = np.array(np.round(f[1]), dtype=int)
            hr_truth, sampling = f[2], f[3]
            below, above = np.sum(hr_truth[sampling] < hr_05), np.sum(hr_truth[sampling] > hr_95)
            arr[i, 0], arr[i, 1], arr[i, 2], arr[i, 3] = below, above, below + above, len(hr_95)
        return pd.DataFrame(data=arr, columns=['below', 'above', 'both', 'total'])


def plot_simulation(result_objs, plot_ci=True, plot_truth=True, ylim=(100, 195)):
    fig = plt.figure()
    ax1, ax2 = fig.subplots(2, 1, sharex=True)

    for result_obj in result_objs:
        sampling = np.array(result_obj.sampling)
        if plot_truth:
            ax1.plot(result_obj.hr_truth, c='tab:red', ls='--')  # plot hr (ground truth)
        mid = pd.DataFrame(result_obj.hr_mu).rolling(2, min_periods=1).mean().values.ravel()
        ax1.plot(sampling,
                 mid,
                 label=f'Predicted HR based on fitness from {result_obj.training_period_lbl}')
        if plot_ci:
            upper = pd.DataFrame(result_obj.hr_95).rolling(2, min_periods=1).mean().values.ravel()
            lower = pd.DataFrame(result_obj.hr_05).rolling(2, min_periods=1).mean().values.ravel()
            ax1.fill_between(sampling,
                             upper,
                             lower,
                             facecolor='gray')
        ax2.plot(pd.DataFrame(result_obj.p).rolling(5, min_periods=1).mean().values, c='C0', ls='-')
    ax1.grid()
    ax1.legend()
    ax1.set_ylabel('Heart Rate [1/min]')
    ax1.set_ylim(ylim)
    ax2.grid()
    ax2.set_ylabel('Power [W]')
    ax2.set_xlabel('Time [s]')


def i_to_kw(kw_prefix, i):
    return kw_prefix + '#' + str(i)


def kw_to_i(kw):
    return int(kw[kw.find('#') + 1:])
